﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XXF.BaseService.ServiceCenter.Service.OpenDoc
{
    /// <summary>
    /// 实体类描述特性
    /// </summary>
    public class EntityDocAttribute:OpenDocAttribute
    {
        public EntityDocAttribute() { }
        public EntityDocAttribute(string text, string description)
            : base(text, description)
        {
 
        }
    }
}
