﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dyd.BaseService.ServiceCenter.Domain.Model
{
    /// <summary>
    /// 搜索基类
    /// </summary>
    public class BaseSearch
    {
        private int pno;
        private int pageSize;
        private string beginDate;
        private string endDate;
        private int defaultPageSize;

        /// <summary>
        /// 开始时间
        /// </summary>
        public string BeginDate
        {
            get
            {
                if (string.IsNullOrEmpty(beginDate))
                {
                    return beginDate;
                }
                else
                {
                    return Convert.ToDateTime(beginDate).ToString("yyyy-MM-dd 00:00:00.001");
                }
            }
            set
            {
                this.beginDate = value;
            }
        }

        /// <summary>
        /// 结算时间
        /// </summary>
        public string EndDate
        {
            get
            {
                if (string.IsNullOrEmpty(endDate))
                {
                    return endDate;
                }
                else
                {
                    return Convert.ToDateTime(endDate).ToString("yyyy-MM-dd 23:59:59.999");
                }
            }
            set
            {
                this.endDate = value;
            }
        }

        /// <summary>
        /// 页面
        /// </summary>
        public int Pno
        {
            get
            {
                if (pno <= 0)
                {
                    return 1;
                }
                else
                {
                    return pno;
                }
            }

            set
            {
                this.pno = value;
            }
        }

        /// <summary>
        /// 页大小
        /// </summary>
        public int PageSize
        {
            get
            {
                if (pageSize <= 0)
                {
                    return DefaultPageSize;
                }
                else
                {
                    return pageSize;
                }
            }
            set
            {
                this.pageSize = value;
            }
        }

        /// <summary>
        /// 默认页大小
        /// </summary>
        public int DefaultPageSize
        {
            get
            {
                return this.defaultPageSize > 0 ? this.defaultPageSize : 12;
            }
            set
            {
                this.defaultPageSize = value;
            }
        }

        /// <summary>
        /// 城市
        /// </summary>
        public int AreaCity
        {
            get;
            set;
        }

        /// <summary>
        /// 店铺
        /// </summary>
        public long AreaShop
        {
            get;
            set;
        }
    }
}
