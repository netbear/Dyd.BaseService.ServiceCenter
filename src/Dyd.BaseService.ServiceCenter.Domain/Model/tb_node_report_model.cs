﻿using Dyd.BaseService.ServiceCenter.Domain.Model;
using System;
using System.ComponentModel.DataAnnotations;

namespace Dyd.BaseService.ServiceCenter.Domain.Model
{
    //tb_node_report
    public class tb_node_report_model
    {

        /// <summary>
        /// id
        /// </summary>
        [Display(Name = "id")]
        public int id { get; set; }
        /// <summary>
        /// 节点id
        /// </summary>
        [Display(Name = "节点id")]
        public int nodeid { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Display(Name = "创建时间")]
        public DateTime createtime { get; set; }
        /// <summary>
        /// 错误数量
        /// </summary>
        [Display(Name = "错误数量")]
        public int errorcount { get; set; }
        /// <summary>
        /// 当前连接数（访问）数量
        /// </summary>
        [Display(Name = "当前连接数（访问）数量")]
        public int connectioncount { get; set; }
        /// <summary>
        /// 访问次数
        /// </summary>
        [Display(Name = "访问次数")]
        public long visitcount { get; set; }
        /// <summary>
        /// 线程数
        /// </summary>
        [Display(Name = "线程数")]
        public int processthreadcount { get; set; }
        /// <summary>
        /// processcpuper
        /// </summary>
        [Display(Name = "processcpuper")]
        public decimal processcpuper { get; set; }
        /// <summary>
        /// 内存大小
        /// </summary>
        [Display(Name = "内存大小")]
        public decimal memorysize { get; set; }

    }

    public class tb_node_report_model_search : BaseSearch
    {
        /// <summary>
        /// 节点id
        /// </summary>
        [Display(Name = "节点id")]
        public int nodeid { get; set; }

        /// <summary>
        /// 查询日期
        /// </summary>
        public string queryDay { get; set; }

        public string TimeChartType { get; set; }

        public string columns { get; set; }

        public string AggregationType { get; set; }

        public string datatype { get; set; }
    }
}