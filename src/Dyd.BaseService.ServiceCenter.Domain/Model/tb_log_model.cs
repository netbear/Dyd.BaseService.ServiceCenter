﻿using Dyd.BaseService.ServiceCenter.Domain.Model;
using System;
using System.ComponentModel.DataAnnotations;

namespace Dyd.BaseService.ServiceCenter.Domain.Model
{
    //tb_log
    public class tb_log
    {
        /// <summary>
        /// id
        /// </summary>
        [Display(Name = "Id")]
        public int id { get; set; }
        /// <summary>
        /// serviceid
        /// </summary>
        [Display(Name = "服务Id")]
        public int serviceid { get; set; }
        /// <summary>
        /// 日志类型:1=服务端,2=客户端,3=系统
        /// </summary>
        [Display(Name = "日志类型")]
        public int logtype { get; set; }
        /// <summary>
        /// msg
        /// </summary>
        [Display(Name = "内容")]
        public string msg { get; set; }
        /// <summary>
        /// createtime
        /// </summary>
        [Display(Name = "记录时间")]
        public DateTime createtime { get; set; }

        [Display(Name = "服务")]
        public string servicename { get; set; }

    }

    //tb_log
    public class tb_log_search : BaseSearch
    {
        /// <summary>
        /// id
        /// </summary>
        [Display(Name = "id")]
        public int id { get; set; }
        /// <summary>
        /// serviceid
        /// </summary>
        [Display(Name = "serviceid")]
        public int serviceid { get; set; }
        /// <summary>
        /// 日志类型:1=服务端,2=客户端,3=系统
        /// </summary>
        [Display(Name = "日志类型:1=服务端,2=客户端,3=系统")]
        public int logtype { get; set; }
        /// <summary>
        /// msg
        /// </summary>
        [Display(Name = "msg")]
        public string msg { get; set; }
        /// <summary>
        /// createtime
        /// </summary>
        [Display(Name = "createtime")]
        public DateTime createtime { get; set; }

    }
}