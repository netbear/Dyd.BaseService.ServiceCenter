﻿using Dyd.BaseService.ServiceCenter.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using XXF.Db;

namespace Dyd.BaseService.ServiceCenter.Web.Core
{
    public class CreateReportTable
    {
        private static readonly string CrateTableSql =
        @"CREATE TABLE [dbo].[{0}](
            [id] [int] IDENTITY(1,1) NOT NULL,
            [nodeid] [int] NOT NULL,
            [createtime] [datetime] NOT NULL,
            [errorcount] [int] NOT NULL,
            [connectioncount] [int] NOT NULL,
            [visitcount] [bigint] NOT NULL,
            [processthreadcount] [int] NOT NULL,
            [processcpuper] [float] NOT NULL,
            [memorysize] [float] NOT NULL
        ) ON [PRIMARY]";

        public static void Run()
        {
            while (true)
            {
                DateTime dt = DateTime.Now.AddDays(-1);
                DateTime endDate = DateTime.Now.AddMonths(1);
                while (dt <= endDate)
                {
                    using (DbConn conn = DbConfig.CreateConn(DataConfig.ServiceCenterReportConnectString))
                    {
                        conn.Open();
                        string tableName = "tb_node_report_" + dt.ToString("yyyyMMdd");
                        if (!conn.TableIsExist(tableName))
                        {
                            string sql = string.Format(CrateTableSql, tableName);
                            List<ProcedureParameter> para = new List<ProcedureParameter>();
                            conn.ExecuteSql(sql, para);
                        }
                        dt = dt.AddDays(1);
                    }
                    Thread.Sleep(1 * 1 * 1 * 1000);
                }
                Thread.Sleep(1 * 1 * 60 * 1000);
            }
        }
    }
}