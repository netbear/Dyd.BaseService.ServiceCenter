﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Dyd.BaseService.ServiceCenter.Test.Service;
using Newtonsoft.Json;
using System.Threading;
using System.Diagnostics;

namespace Dyd.BaseService.ServiceCenter.Test.Client
{
    public partial class FormMain : Form
    {
        public static int Sum { get; set; }
        public static int finishCount = 0;
        public static int CallCount = 0;
        public static int ThreadCount = 0;
        public static long SuccessCount = 0;
        public static bool JustCall = true;

        public FormMain()
        {
            InitializeComponent();
        }

        private void btnCall_Click(object sender, EventArgs e)
        {
            //XXF.BaseService.ServiceCenter.Client.Sdk.SDKProvider sdk = new XXF.BaseService.ServiceCenter.Client.Sdk.SDKProvider();
            //sdk.ToLocalFile("Dyd.BaseService.ServiceCenter.Test.Service", @"C:\Users\Administrator\Documents\Visual Studio 2012\Projects\Dyd.BaseService.ServiceCenter\Dyd.BaseService.ServiceCenter.Test.Client\ThirdServices\Dyd.BaseService.ServiceCenter.Test.Service");
            var client = new IMyServiceClient();
            this.richTextBox1.Text = JsonConvert.SerializeObject(client.ToDo2(new MyEntity2()
            {
                P10 = "10",
                P2 = new List<int>() { 1, 2 },
                P3 = new List<byte>() { 1, 2 },
                P4 = new Dictionary<string, string>() { { "4", "4" } },
                P5 = true,
                P6 = 66666,
                P7 = 1,
                P8 = 2,
                P9 = new byte[] { 1, 2 }
            }));
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            this.richTextBox1.Text = "";
        }

        private void btnMutiCall_Click(object sender, EventArgs e)
        {
            finishCount = 0;
            CallCount = Convert.ToInt32(this.textBoxCallCount.Text.Trim());
            ThreadCount = Convert.ToInt32(this.textBoxCallThread.Text.Trim());
            Stopwatch sw = new Stopwatch();
            sw.Start();
            for (int i = 0; i < ThreadCount; i++)
            {
                System.Threading.Thread td = new System.Threading.Thread(DoSomeWorkThread);
                td.IsBackground = true;
                td.Name = "thread_" + i;
                td.Start();
            }
            while (true)
            {
                if (ThreadCount == finishCount)
                {
                    this.richTextBox1.Text = sw.ElapsedMilliseconds.ToString();
                    break;
                }
                Thread.Sleep(50);
            }
            Console.WriteLine(SuccessCount);
        }

        static void DoSomeWorkThread()
        {
            int count = CallCount / ThreadCount;
            int j = 1;
            while (j > 0)
            {
                var client = new IMyServiceClient();
                while (count > 0)
                {
                    var pare = new MyEntity2()
                    {
                        P10 = "10",
                        P2 = new List<int>() { 1, 2 },
                        P3 = new List<byte>() { 1, 2 },
                        P4 = new Dictionary<string, string>() { { "4", "4" } },
                        P5 = true,
                        P6 = 66666,
                        P7 = 1,
                        P8 = 2,
                        P9 = new byte[] { 1, 2 }
                    };
                    client.ToDo2(pare);
                    Sum++;
                    count--;
                }
                count = CallCount / ThreadCount; ;
                j--;
            }

            Interlocked.Increment(ref finishCount);
        }
    }
}
